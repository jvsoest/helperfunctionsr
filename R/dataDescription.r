
tableDescription <- function(dataVector) {
  result <- table(dataVector, useNA="always")
  
  resultPercentage <- (table(dataVector, useNA = "always")/sum(table(dataVector, useNA = "always")))*100
  
  rbind(result, resultPercentage)
}

loadTypeDependentResults <- function(dataVector) {
  if(sum(class(dataVector) %in% c("factor", "logical", "character"))>0) {
    return(tableDescription(dataVector))
  }
  
  if(class(dataVector) %in% c("numeric", "integer")) {
    return(data.frame(mean=mean(dataVector, na.rm=TRUE), 
                      sd=sd(dataVector, na.rm=TRUE),
                      min=min(dataVector, na.rm=TRUE),
                      median=median(dataVector, na.rm=TRUE),
                      max=max(dataVector, na.rm=TRUE),
                      amountNA=sum(is.na(dataVector))))
  }
}

castVars <- function(dataFrame, castTypes) {
  columnsToCast <- which(!is.na(castTypes))
  for (i in columnsToCast) {
    if(castTypes[i]=="factor")
      dataFrame[,i] <- as.factor(dataFrame[,i])
    if(castTypes[i]=="logical")
      dataFrame[,i] <- dataFrame[,i]==1
  }
  
  dataFrame
}

analyzeDataFrame <- function(dataFrame, castTypes=c(NA)) {
  
  dataFrame <- castVars(dataFrame, castTypes)
  
  lapply(dataFrame, loadTypeDependentResults)
}

compareColumn <- function(dataFrameA, dataFrameB, colName) {
  if(class(dataFrameA[,colName]) %in% c("numeric", "integer")) {
    return(wilcox.test(x=dataFrameA[,colName], y=dataFrameB[,colName], alternative="two.sided"))
  }
  
  if(class(dataFrameA[,colName]) %in% c("factor", "logical")) {
    
    if(class(dataFrameA[,colName])=="factor") {
      uniqueOptions = unique(levels(dataFrameA[,colName]), levels(dataFrameB[,colName]))
      dataFrameA[,colName] <- factor(dataFrameA[,colName], levels=uniqueOptions)
      dataFrameB[,colName] <- factor(dataFrameB[,colName], levels=uniqueOptions)
    }
    
    contingencyTable = rbind(table(dataFrameA[,colName]), table(dataFrameB[,colName]))
    return(chisq.test(contingencyTable))
    #return(fisher.test(rbind(table(dataFrameA[,colName]), table(dataFrameB[,colName])))) #non-parimetric chisq.test
  }
}

compareDataFrames <- function(dataFrameList, castTypes=c(NA)) {
  if(sum(sort(colnames(dataFrameList[[1]]))!=sort(colnames(dataFrameList[[2]])))>0) {
    warning("Data frame column names are not equal");
    return;
  }
  
  for(i in 1:length(dataFrameList)) {
    dataFrameList[[i]] <- castVars(dataFrameList[[i]], castTypes)
  }
    
  lapply(colnames(dataFrameList[[1]]), function(x) {list(variable=x, comparison=compareColumn(dataFrameList[[1]], dataFrameList[[2]], x))})
}