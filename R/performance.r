library(ROCR)
library(verification)
library(Hmisc)

###################
# Plot the ROC curve for the predicted and observed outcomes
# This plot is performed on the current device.
# INPUT PARAMETERS:  
#  - predicted: the predicted values
#	 - observed: the observed outcome, binary
#  - fileName: OPTIONAL, file to plot the result to (as PNG), 
#      otherwise it's plotted to the active device
#  - plotLegend: OPTIONAL, legend containing AUC, events and
#      sample size are plotted (TRUE). Can be turned off (FALSE)
###################
plotROC <- function(predicted, observed, fileName=NA, plotLegend=TRUE, title="ROC curve") {
  
  data <- data.frame(pred=predicted, obs=observed)
  data <- data[complete.cases(data),]
  predicted <- data$pred
  observed <- data$obs
  
  if(!is.na(fileName)) {
    png(filename = fileName, width = 500, height=500)
  }
  
  if(length(unique(observed))==2) {
    pred <- prediction(predicted, observed);
    perf <- performance(pred, measure="tpr", x.measure="fpr");
  
    plot(perf, col=rainbow(10), main=title, xlab="1-Specificity", ylab="Sensitivity", lwd=3);
  } else {
    plot(x=c(0,1), y=c(0,1), col="white", main="ROC curve", xlab="1-Specificity", ylab="Sensitivity", lwd=3);
    message("Warning, could not calculate an ROC curve, observations do not match 2 classes.")
  }
  abline(0,1,col="gray", lwd=2);
  if(plotLegend) {
    legend(x=0.5, y=0.4, c(paste0("AUC: ", calculateAuc(data.frame(pred=predicted, outcome=observed))),
                           paste0("events: ", sum(observed, na.rm=TRUE)),
                           paste0("size: ", length(observed))
                           ))
  }
  
  if(!is.na(fileName)) {
    dev.off()
  }
  
}

###################
# Creates a subset of the dataset, using the given indices. If no indices available,
#   return complete dataset
# INPUT PARAMETERS:
#  - data: data.frame object containing the dataset
#	- indices: vector of row numbers to use (optional)
###################
createSubset <- function(data,indices=NA) {
  #if indices is not larger than one, and indices is NA, create list of indices to use
  if(length(indices)==1) {
    if(is.na(indices)) {
      data <- data[complete.cases(data),]
      indices <- c(1:nrow(data))
    }
  }
  
  data[indices,];
}

###################
# Calculates the Area under ROC curve, based on the data elements given
# INPUT PARAMETERS:
#	- data: data.frame object containing the following columns:
#		- pred: the predicted values
#		- outcome: the observed outcome, binary
#	- indices: vector of row numbers of the data object to calculate the AUC on (optional)
###################
calculateAuc <- function(data,indices=NA) {
  #create the used subset
  bootData <- createSubset(data,indices)
  
  if(length(unique(bootData$outcome))>1) {
    somers2(bootData$pred, bootData$outcome)["C"]
  } else {
  	NA
  }
}

###################
# Calculates the Brier score, based on the data elements given
# INPUT PARAMETERS:
#  - data: data.frame object containing the following columns:
#		- pred: the predicted values
#		- outcome: the observed outcome, binary
#	- indices: vector of row numbers of the data object to calculate the Brier score on (optional)
###################
calculateBrier <- function(data,indices=NA) {
  #create the used subset
  bootData <- createSubset(data,indices)
  
  if(length(unique(bootData$outcome))>1) {
    brier(bootData$outcome, bootData$pred)$bs;
  } else {
    NA
  }
}

###################
# Calculates All performance metrics
# INPUT PARAMETERS:
#  - data: data.frame object containing the following columns:
#  	- pred: the predicted values
#		- outcome: the observed outcome, binary
#	- indices: vector of row numbers of the data object to calculate the performance metrics on (optional)
###################
calculatePerformanceMetrics <- function(data, indices=NA) {
  myData = createSubset(data, indices)
  sSize = nrow(myData)
  
  c(auc=calculateAuc(data, indices)[1], brier=calculateBrier(data, indices), sampleSize=sSize);
}