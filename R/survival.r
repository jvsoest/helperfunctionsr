library(survival)

######################################################################
# rightCensoredToBinary: make binary information out of right censored
#   data
# INPUT:
#   - event: the actual binary outcome event
#   - timepoint: the timepoint on which the binary outcome event
#       occured
#   - targetTimepoint: timepoint which has to be met, if event is
#        positive
# OUTPUT:
#   - binary vector containing the binary outcome based on right-
#        censored information
######################################################################
rightCensoredToBinary <- function(event, timepoint, targetTimepoint) {
  output <- event
  output[(event & (timepoint > targetTimepoint))] <- FALSE #event is reached AFTER timepoint, so no event at timepoint
  output[((!event) & (timepoint < targetTimepoint))] <- NA #lost in follow up, no event but before target timepoint
  output
}

######################################################################
# predictSurvTimePointPatient: retrieves the survival probability at a 
#   certain timepoint for a given patient survival curve
# INPUT:
#   - sfit: survfit object
#   - timepoint: prediction on this certain timepoint
# OUTPUT:
#   - survival probability for all cases
######################################################################
predictSurvTimePointPatient <- function(sfit, timepoint, doPlot=FALSE) {
  index <- which(sfit$time>=timepoint)[1]
  hazard <- sfit$surv[index]
  
  if(doPlot) {
    plot(sfit)
    abline(v=timepoint, col="red")
    abline(h=hazard, col="red")
  }
  
  hazard
}

######################################################################
# predictSurvTimePoint: retrieves the survival probability at a 
#   certain timepoint
# INPUT:
#   - survList: List object containing survfit objects for every patient
#   - timepoint: prediction on this certain timepoint
# OUTPUT:
#   - survival probability for all cases
######################################################################
predictSurvTimePoint <- function(survList, timepoint) {
  innerFunction <- function(x, survList, timepoint) {
    predictSurvTimePointPatient(survList[[x]], timepoint)
  }
  
  sapply(c(1:length(survList)), innerFunction, survList, timepoint)
}

######################################################################
# predictSurvAll: predicts the survival curve for a specific case
# INPUT:
#   - model: cox proportional hazards model to be executed
#   - dataSet: the dataset where the index x is applied to
# OUTPUT:
#   - list containing survfit objects for every patient
######################################################################
predictSurvAll <- function(model, dataSet) {
  patientsList <- lapply(c(1:nrow(dataSet)), function(x, model, dataSet) {survfit(model, dataSet[x,])}, model, dataSet)
  patientsList
}

###################################################################################
# predictSurv: predicts the survival curve for all patients for a given timepoint
# INPUT:
#   - model: cox proportional hazards model to be executed
#   - dataSet: the dataset where the index x is applied to
# OUTPUT:
#   - list containing survfit objects for every patient
###################################################################################
predictSurv <- function(model, dataSet, timepoint) {
  survList <- predictSurvAll(model, dataSet);
  prob <- predictSurvTimePoint(survList, timepoint);
  prob
}

######################################################################
# predictOnTimepoint: predicts the survival for a specific dataset for
#   a given model at a given timepoint
# INPUT:
#   - model: cox proportional hazards model to be executed
#   - dataSet: the dataSet to apply the prediction model to
#   - timepoint: the timepoint to predict for
#   - binaryOutcomeVar: string containing the binary outcome variable
#       column name in dataSet
#   - timeOutcomeVar: the timepoint at which the binaryOutcomeVar was
#       measured (column name)
# OUTPUT:
#   - data.frame containing the binary censored outcome
#       (column name: outcome) and the predicted outcome 
#       (column name: pred); independently calculated, not based on
#       binary censored outcome
######################################################################
predictOnTimepoint <- function(model, dataSet, timepoint, binaryOutcomeVar, timeOutcomeVar) {
  #determine binary outcome, and filter (NA) non-events before outcome
  censoredBinary <- rightCensoredToBinary(dataSet[,binaryOutcomeVar], dataSet[,timeOutcomeVar], timepoint)
  
  #add column to dataset
  dataSet$outcome <- censoredBinary
  
  #remove NA's
  dataSet <- dataSet[complete.cases(dataSet),]
  
  #run predictSurv for every row in dataSet
  dataSet$pred <- predictSurv(model, dataSet, timepoint)
  
  #as we're predicting survival, we want to know the opposite (chance on event)
  dataSet$pred <- 1-dataSet$pred
  
  dataSet[,c("pred", "outcome")]
}