library(rms)

###################
# Plot a nomogram for a logistic regression model, based on GLM
# See ?glm for more information on creating a model
# INPUT PARAMETERS:  
#  - myModel: the model used to develop the nomogram on
#	 - x: timepoint for output (only used in title)
###################
plotNomogramFromGLM <- function(myModel, x=NA, title="Logistic nomogram FU month ") {
  #get dataset model was trained on
  myDataSet <- myModel$model;
  
  #set global environment variable containing dataset metadata
  dd <<- datadist(myDataSet)
  options(datadist="dd")
  
  #learn logistic model again
  myModelNew <- lrm(outcome ~ ., data=myDataSet)
  
  #build nomogram plotting information object
  myNomogram <- nomogram(myModelNew, fun=plogis, lp=FALSE)
  
  #remove global environment variable
  options(datadist=NULL)
  rm(list = c("dd"), pos = ".GlobalEnv")
  
  #plot actual nomogram
  plot(myNomogram)
  if(!exists("x")) {
    title <- paste0(title, x)
  }
  title(title)
  
  #return nomogram plotting information and lrm model
  list(myNomogram, myModelNew)
}

###################
# Plot a nomogram for a Cox Proportional hazards model, based on coxph
# See ?coxph for more information on creating a model
# INPUT PARAMETERS:  
#	 - x: timepoint for nomogram prediction
#  - coxResult: List containing three items:
#       [[1]]: model trained (instance of coxph class)
#       [[2]]: (not important for nomogram) vector of trial names where data came from
#       [[3]]: dataset used for model development
#	 - outcomeTime: variable indicating the censored outcome time
#	 - outcome: binary value indicating the actual information
###################
plotNomogramCoxFromGLM <- function(x, coxResult, outcomeTime, outcome) {
  #read dataset
  mySet <- coxResult[[3]]
  
  #create survival object
  survSet <- Surv(mySet[,outcomeTime], mySet[,outcome], type="right")
  
  #remove outcome columns from dataset
  mySet <- mySet[,which(!(colnames(mySet) %in% c(outcomeTime, outcome)))]
  
  #train cph cox model (gives same results)
  myCox <- cph(survSet ~ ., data=mySet, surv=TRUE)
  
  #assign a datadist object (indicating dataset metadata) to global environment
  dd <<- datadist(mySet)
  options(datadist="dd")
  
  #generate survival function for nomogram application, x is the timepoint to build the
  # nomogram for
  surv <- Survival(myCox)
  survTime <- function(lp) { 1 - surv(x, lp) }
  
  #build actual nomogram plotting information
  myNomogram <- nomogram(myCox, fun=survTime, lp=FALSE)
  
  #remove global environment variable
  options(datadist=NULL)
  rm(list = c("dd"), pos = ".GlobalEnv")
  
  #plot nomogram and set title
  plot(myNomogram)
  title(paste0("Cox PH nomogram FU month ", x))
  
  #return list containing nomogram plot info and new cox model
  list(myNomogram, myCox)
}