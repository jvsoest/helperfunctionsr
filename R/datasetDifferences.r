# Implementation of the Membership model as published by Debray et al. 2015

######################################################################################
# Function to calculate how different datasets are. A high AUC indicates a good
#   discriminative ability to predict whether a case (row) belongs to cohort A or B.
#   An AUC of 0.5 indicates no discriminative ability, implying homogeneous datasets.
# INPUT:
#   - dataSet: data.frame object containing both datasets
#   - modelVars: vector containing the column names of dataSet, included for comparison
#   - cohortVar: variable name indicating the column stratifying between cohorts
# OUTPUT:
#   - list object:
#     $model: model object of cohort differnces result
#     $auc: actual AUC calculated on dataset
#     $bootAucMean: bootstrapped AUC (R=rows*1000) mean
#     $bootAucStd: bootstrapped AUC (R=rows*1000) standard deviation
######################################################################################
calculateCohortDifference <- function(dataSet, modelVars, cohortVar, plotResult=FALSE, bootstrap=FALSE) {
  membershipSet <- dataSet[,modelVars]
  membershipSet$outcome <- dataSet[,cohortVar]
  
  if("factor" %in% class(membershipSet$outcome)) {
    dataSet[,cohortVar] <- as.character(dataSet[,cohortVar])==levels(dataSet[,cohortVar])[1]
    membershipSet$outcome <- as.character(membershipSet$outcome)==levels(membershipSet$outcome)[1]
  }
  
  glmOutcome <- glm(outcome ~ ., family=binomial, data=membershipSet);
  membershipAuc <- calculateAuc(data.frame(pred=glmOutcome$linear.predictors,outcome=glmOutcome$y==1))
  
  returnList <- list(model=glmOutcome, auc=membershipAuc)
  
  membershipTrain <- function(membershipSet, indices) {
    mySet <- membershipSet[indices,]
    myGlm <- glm(outcome ~ ., family=binomial, data=mySet);
    
    calculateAuc(data.frame(pred=myGlm$linear.predictors, outcome=myGlm$y==1))
  }
  
  if(bootstrap) {
    membershipPerformanceBoot <- boot(membershipSet, membershipTrain, R=nrow(membershipSet)*10)
    returnList$bootAucMean <- mean(membershipPerformanceBoot$t)
    returnList$bootAucSd <- sd(membershipPerformanceBoot$t)
  }
  
  if("lp" %in% colnames(dataSet)) {
    trainingLp <- dataSet$lp[!dataSet[,cohortVar]]
    validationLp <- dataSet$lp[dataSet[,cohortVar]]
    dataFrameObj <- data.frame(meanTraining=mean(trainingLp, na.rm = TRUE),
                               meanValidation=mean(validationLp, na.rm = TRUE),
                               sdTraining=sd(trainingLp, na.rm=TRUE),
                               sdValidation=sd(validationLp, na.rm=TRUE),
                               cdAuc=membershipAuc)
    
    returnList$membershipPlotParams <- dataFrameObj
    returnList$membershipPlot <- plotCohortDifferences(dataFrameObj)
  }
  
  returnList
}

#############################################################
# Plot both cohort differences graphs on current device     #
# Input:                                                    #
#    dataFrameObj (data frame):                             #
#        $meanTraining: probabilities of validation set     #
#        $meanValidation: probabilities of training set     #
#        $sdTraining: probabilities of validation set       #
#        $sdValidation: probabilities of training set       #
#    	   $auc: AUC of membership model                      #
#############################################################
plotCohortDifferences <- function(dataFrameObj) {
  meanList <- data.frame(training=dataFrameObj$meanTraining)
  meanList$validation <- dataFrameObj$meanValidation
  meanList$auc <- dataFrameObj$cdAuc
  
  sdList <- data.frame(training=dataFrameObj$sdTraining)
  sdList$validation <- dataFrameObj$sdValidation
  sdList$auc <- dataFrameObj$cdAuc
  
  grid.arrange(plotCohortDifferencesMeans(meanList, backgroundColor="black"), plotCohortDifferencesRatioSD(sdList, backgroundColor="black"), ncol=2, heights=unit(300, "points"))
}

#plotCohortDifferences(data.frame(meanTraining=0.8, meanValidation=0.7, sdTraining=0.3, sdValidation=0.28, cdAuc=0.73))

########################################################
# Plot the ratio of standard deviations and AUCs       #
# Input:                                               #
#    standardDeviations (data frame):                  #
#        $validation: probabilities of validation set  #
#        $training: probabilities of training set      #
#    	   $auc: AUC of membership model                 #
########################################################
plotCohortDifferencesRatioSD <- function(standardDeviations, backgroundColor="red") {
  x <- standardDeviations$validation / standardDeviations$training;
  y <- standardDeviations$auc;
  
  xBounds.min <- (floor(min(x)*10)/10)-0.1;
  xBounds.max <- (ceiling(max(x)*10)/10)+0.1;
  xBounds.steps <- 0.1;
  yBounds.min <- 0.4;
  yBounds.max <- 1.0;
  yBounds.steps <- 0.1;
  
  if(xBounds.min > 1)
    xBounds.min <- (1 - 0.1);
  
  if(xBounds.max < 1)
    xBounds.max <- (1 + 0.1);
  
  if(yBounds.min > 0.5)
    yBounds.min <- 0.4;
  
  if(yBounds.max < 0.5)
    yBounds.max <- 0.6
  
  xGrid = seq(xBounds.min, xBounds.max, by=0.005)
  yGrid = seq(yBounds.min, yBounds.max, by=0.005)
  d <- expand.grid(x=xGrid, y=yGrid)
  
  #fill ranges from -1 to 1 with a center of 0
  g <- ggplot(d, aes(x, y, fill=sqrt((((x-1)-0)^2) + (((y-0.5)-0)^2))^1.75)) + #create a plot with a grid fill, grid fill should be white at center, and more red on outside (using euclidean distance)
    geom_tile() + #using "tiles" to show this grid fill
    scale_fill_gradient2(high=backgroundColor, low=backgroundColor, mid="white", midpoint=0) + #use a gradient on the fill values
    theme(legend.position="none", #do not show a legend
          axis.line=element_line(color="black"), #color the axis line black
          axis.text=element_text(color="black") #color the text on the axis line black
    ) +
    coord_fixed(xlim=c(xBounds.min, xBounds.max), ylim=c(yBounds.min, yBounds.max)) + #fix the coordinates of this graph
    geom_vline(xintercept = 1, col="gray") + #show a gray line (vertical) for the crosshair
    geom_hline(yintercept = 0.5, col="gray") + #show a gray line (horizontal) for the crosshair
    xlab("SD(lp_validation) / SD(lp_training)") +
    ylab("Dataset Differences AUC");
  
  
  g <- g + geom_point(data=data.frame(x=x, y=y), aes(x=x, y=y), shape=c(1:length(x)), size=5, color="green"); #plot the points on the graph
  
  g
  
}

###########################################################
# Plot difference in means and AUCs                       #
# Input:                                                  #
#    means (data frame):                                  #
#        $validation: mean probability in validation set  #
#        $training: mean probability in training set      #
#    	 $auc: AUC of membership model                      #
###########################################################
plotCohortDifferencesMeans <- function(means, backgroundColor="red") {
  x <- means$validation - means$training;
  y <- means$auc;
  
  xBounds.min <- (floor(min(x)*10)/10)-0.1;
  xBounds.max <- (ceiling(max(x)*10)/10)+0.1;
  xBounds.steps <- 0.1;
  yBounds.min <- 0.4;
  yBounds.max <- 1.0;
  yBounds.steps <- 0.1;
  
  if(xBounds.min > 0)
    xBounds.min <- -0.1;
  
  if(xBounds.max < 0)
    xBounds.max <- 0.1;
  
  if(yBounds.min > 0.5)
    yBounds.min <- 0.4;
  
  if(yBounds.max < 0.5)
    yBounds.max <- 0.6
  
  xGrid = seq(xBounds.min, xBounds.max, by=0.005)
  yGrid = seq(yBounds.min, yBounds.max, by=0.005)
  d <- expand.grid(x=xGrid, y=yGrid)
  
  #fill ranges from -1 to 1 with a center of 0
  g <- ggplot() +#create a plot
    geom_tile(data=d, aes(x, y, fill=sqrt((((x-0)-0)^2) + (((y-0.5)-0)^2))^1.75)) + #using "tiles" to show this grid fill
    scale_fill_gradient2(high=backgroundColor, low=backgroundColor, mid="white", midpoint=0, guide=FALSE) + #use a gradient on the fill values
    theme(legend.position="right", #do not show a legend
          axis.line=element_line(color="black"), #color the axis line black
          axis.text=element_text(color="black") #color the text on the axis line black
    ) +
    coord_fixed(xlim=c(xBounds.min, xBounds.max), ylim=c(yBounds.min, yBounds.max)) + #fix the coordinates of this graph
    geom_vline(xintercept = 0, col="gray") + #show a gray line (vertical) for the crosshair
    geom_hline(yintercept = 0.5, col="gray") + #show a gray line (horizontal) for the crosshair
    
    xlab("Mean(lp_validation) - Mean(lp_training)") +
    ylab("Dataset Differences AUC");
  
  g <- g + geom_point(data=melt(data.frame(x=x, y=y), id.var="y"), aes(x=value, y=y), shape=c(1:length(x)), size=5, show.legend=TRUE, color="green"); #plot the points on the graph
  
  g
}

###########################################################
# Plot dummy graph where the legend can be extracted from #
# Input:                                                  #
#    labelNames: Vector containing the names of different #
#            points in dataset                            #
###########################################################
plotCohortDifferencesLegend <- function(labelNames = c("unknown")) {
  plot(x=c(1:length(labelNames)), pch=c(1:length(labelNames)))
  legend(x=1, y=length(labelNames), legend=labelNames, pch=1:length(labelNames), title="Legend:")
}