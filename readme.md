#Helper functions R

This repository holds additional functions, next to the packages already available in R.
These functions can be seen as "convenience functions" to make your code more readable, and understandable
for other people.

Currently, this repository holds the following files:

* [R/performance.r](R/performance.r): defining functions to determine the performance of probabilities compared to observed values
* [R/survival.r](R/survival.r): defining functions to determine the probability based on a cox proportional hazards model,
Plot Kaplan-Meier curves for specific cases, or to make right-censored information binary.
* [R/datasetDifferences.r](R/datasetDifferences.r): Method by Debray et al. to calculate cohort differences, implemented with visualization.

## Use of these helper functions

To use these helper functions, it is mandatory to load every specific file individually. There are currently no dependencies between files.
Mind that the location of the file is important, the example below states that performance.r should be in the current working directory (see [https://stat.ethz.ch/R-manual/R-devel/library/base/html/getwd.html](https://stat.ethz.ch/R-manual/R-devel/library/base/html/getwd.html)).

To load performance.r, please use the following line:
> source("R/performance.r")

If this repository is checked out at C:\repos\helperFunctionsR, then loading performance.r would look like this:
> source("C:/repos/helperFunctionsR/performance.r")

Finally, using home directories makes life easier. For example, when storing the repository in C:\Users\<username>\Documents\Repositories\helperFunctionsR (or /Users/username/Repositories/helperFunctionsR on OSX, or /home/username/Repositories/helperFunctionsR on Linux) will result in:
> source("~/Repositories/helperFunctionsR/R/performance.r")

## Performance functions ([R/performance.r](R/performance.r))

This file holds functions to determine the performance of probabilities when compared to observations. The following functions are available:

* plotROC: plots the ROC curve based on given probabilities and binary outcome
* calculateAuc: Calculates the AUC on the given data.frame (matrix), or subset (if indices is specified). The later can be used in combination with the bootstrap package.

## Survival functions ([R/survival.r](R/survival.r))

This file holds many convenience functions for working with survival (right-censored) data, including cox proportional hazard models.

Functions:

* rightCensoredToBinary: Make binary outcome at a specified timepoint, where data is originating from an event, and the timepoint of this event.
* predictSurvTimePointPatient: Retrieves the survival probability at a certain timepoint for a given patient by it's specific kaplan-meijer plot (survfit object). It optionally can plot the kaplan meijer curve for this specific patient.
* predictSurvTimePoint: Retrieves the survival probability at a certain timepoint for a list of patients (represented by their survfit objects).
* predictSurvAll: Precicts the survival curve for specific set of patients on a specific cox proportional hazards model. Returns a list of survival curves (survfit objects), each for every patient.
* predictOnTimepoint: complementary function to predict the survival for a specific model, dataset and timepoint (aggregation function of all functions described before), and results a data.frame containing predicted and observed outcomes for this specific timepoint.

## Cohort differences [R/datasetDifferences.r](R/datasetDifferences.r)

This file holds the code to perform cohort differences, as described by Debray et al. It compares two data.frame objects with equal column names (and column types).